import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:task_desgin/screen/add_products_screen.dart';
import 'package:task_desgin/utils/app_colors.dart';
import 'package:task_desgin/widget/my_text.dart';

class ProductsScreen extends StatefulWidget {
  const ProductsScreen({Key? key}) : super(key: key);

  @override
  State<ProductsScreen> createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  int indexColor = 0;
  int indexList = 0;
  List<String> listText1 = [
    'تصنيف 1',
    'تصنيف 2',
    'تصنيف 3',
    'تصنيف 4',
  ];
  List<String> listImage1 = [
    'images/Rectangle1.png',
    'images/Rectangle2.png',
    'images/Rectangle3.png',
    'images/Rectangle1.png',
  ];
  List<String> listImage2 = [
    'images/pro.png',
    'images/p1.png',
    'images/p2.png',
    'images/p3.png',
  ];
  List<String> listText2 = [
    'تصنيف 1',
    'تصنيف 2',
    'تصنيف 3',
    'تصنيف 4',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.White_COLOR,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.White_COLOR,
        title: MyText(
            text: 'المنتجات',
            fontSize: 20.sp,
            fontWeight: FontWeight.bold,
            color: AppColors.COLOR),
        actions: [
          Container(
              height: 30.h,
              width: 40.w,
              margin: EdgeInsets.only(left: 10.w, bottom: 7.h,top: 7.h),
              decoration: BoxDecoration(
                  color: AppColors.White_COLOR,
                  borderRadius: BorderRadius.circular(12.r),
                  boxShadow: [
                    BoxShadow(spreadRadius: 1.5, color: Colors.black12)
                  ]),
              child: IconButton(
                  onPressed: () => Get.to(AddProductsScreen()),
                  icon: Icon(
                    Icons.add,
                    color: AppColors.BLack_COLOR,
                  ))),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView(
          children: [
            MyText(
                text: 'التصنيفات',
                fontSize: 16.sp,
                fontWeight: FontWeight.w600,
                color: AppColors.COLOR),
            SizedBox(height: 8.h),
            SizedBox(
              width: 300.w,
              child: Row(
                children: [
                  SizedBox(
                    width: 120.w,
                    child: Row(
                      children: [
                        SizedBox(width: 1.w),
                        InkWell(
                          onTap: () {
                            indexColor == 0 ? indexColor = 1 : indexColor = 0;
                            setState(() {});
                          },
                          child: Container(
                            width: 110.w,
                            height: 126.h,
                            // alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: AppColors.White_COLOR1,
                                borderRadius: BorderRadius.circular(20.h),
                                boxShadow: [
                                  BoxShadow(
                                      color: indexColor == 1
                                          ? AppColors.White_COLOR1
                                          : AppColors.Green_COLOR,
                                      spreadRadius: 1)
                                ]),

                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(15.r),
                                  child: Image.asset(
                                    'images/pro2.png',
                                    width: 92.w,
                                    fit: BoxFit.fill,
                                    height: 79.h,
                                  ),
                                ),
                                SizedBox(height: 8.h),
                                MyText(
                                    text: 'عرض الكل',
                                    fontSize: 12.sp,
                                    fontWeight: FontWeight.w600,
                                    color: AppColors.COLOR),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  indexColor == 0
                      ? Expanded(
                          child: SizedBox(
                            height: 130.h,
                            child: ListView.builder(
                                physics: ScrollPhysics(),
                                scrollDirection: Axis.horizontal,
                                itemCount: 4,
                                itemBuilder: (BuildContext context, int index) {
                                  return AnimationConfiguration.staggeredGrid(
                                    columnCount: index,
                                    position: index,
                                    duration: const Duration(milliseconds: 375),
                                    child: SlideAnimation(
                                      verticalOffset: 50.0,
                                      child: FadeInAnimation(
                                        child: Padding(
                                          padding: EdgeInsets.symmetric(horizontal: 8.w),
                                          child: Row(
                                            children: [
                                              SizedBox(width: 1.w),
                                              Container(
                                                width: 110.w,
                                                height: 126.h,
                                                // alignment: Alignment.center,
                                                decoration: BoxDecoration(
                                                  color: AppColors.White_COLOR1,
                                                  borderRadius:
                                                  BorderRadius.circular(20.h),
                                                ),

                                                child: Column(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                                  children: [
                                                    ClipRRect(
                                                      borderRadius:
                                                      BorderRadius.circular(15.r),
                                                      child: Image.asset(
                                                        listImage1[index],
                                                        width: 92.w,
                                                        fit: BoxFit.fill,
                                                        height: 79.h,
                                                      ),
                                                    ),
                                                    SizedBox(height: 8.h),
                                                    MyText(
                                                        text: listText1[index],
                                                        fontSize: 12.sp,
                                                        fontWeight: FontWeight.w600,
                                                        color: AppColors.COLOR),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                }),
                          ),
                        )
                      : SizedBox(),
                ],
              ),
            ),
            SizedBox(height: 15.h),
            Row(
              children: [
                Spacer(),
                InkWell(
                  onTap: () {
                    indexList == 0 ? indexList = 1 : indexList = 0;
                    setState(() {});
                  },
                  child: Container(
                    height: 36.h,
                    width: 220.w,
                    alignment: Alignment.center,
                    margin: EdgeInsets.only(left: 10.w, bottom: 10.h),
                    decoration: BoxDecoration(
                      color: AppColors.White_COLOR1,
                      borderRadius: BorderRadius.circular(8.r),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          indexList == 1? Icons.list_alt: Icons.dataset_outlined,
                          color: AppColors.Red_COLOR,
                        ),
                        SizedBox(width: 10.w),
                        MyText(
                            text:          indexList == 0?  'تغيير عرض المنتجات الى افقي':'تغيير عرض المنتجات الى عمودي',
                            fontSize: 12.sp,
                            fontWeight: FontWeight.w400,
                            color: AppColors.Red_COLOR),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.h),
            indexList == 0
                ? ListView.builder(
                    shrinkWrap: true,
                    // controller: PageController(viewportFraction: 2.34),
                    physics: ScrollPhysics(),
                    scrollDirection: Axis.vertical,
                    // padding: EdgeInsets.symmetric(vertical: 10.h),
                    itemCount: 4,
                    itemBuilder: (BuildContext context, int index) {
                      return AnimationConfiguration.staggeredList(
                        position: index,
                        duration: const Duration(milliseconds: 375),
                        child: SlideAnimation(
                          verticalOffset: 50.0,
                          child: FadeInAnimation(
                            child: Padding(
                              padding: EdgeInsets.symmetric(vertical: 10.h),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    listImage2[index],
                                    height: 115.h,
                                  ),
                                  SizedBox(width: 14.w),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      // SizedBox(height: 7.h),

                                      MyText(
                                          text: 'هذا النص هو مثال لنص',
                                          fontSize: 18.sp,
                                          fontWeight: FontWeight.w600,
                                          color: AppColors.BLack_COLOR),
                                      SizedBox(height: 8.h),

                                      Row(
                                        children: [
                                          MyText(
                                              text: '120',
                                              fontSize: 20.sp,
                                              fontWeight: FontWeight.w600,
                                              color: AppColors.Green_COLOR),
                                          SizedBox(width: 7.w),
                                          MyText(
                                              text: 'دولار',
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w400,
                                              color: AppColors.COLOR),
                                        ],
                                      ),
                                      SizedBox(height: 14.h),

                                      Container(
                                        height: 31.h,
                                        width: 61.w,
                                        alignment: Alignment.center,

                                        decoration: BoxDecoration(
                                          color: AppColors.BackSel_COLOR,
                                          borderRadius: BorderRadius.circular(8.r),
                                        ),
                                        child: MyText(
                                            text: 'اسم المتجر',
                                            fontSize: 12.sp,
                                            fontWeight: FontWeight.w300,
                                            color: AppColors.Sel_COLOR),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 7.h),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    })
                : SizedBox(
                    // height: 200,
                    child: GridView.builder(
                      shrinkWrap: true,
                      // controller: PageController(viewportFraction: 2.34),
                      physics: ScrollPhysics(),

                      itemCount: 4,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 10.w,
                        mainAxisSpacing: 17.w,
                        childAspectRatio: 182.h / 230.w,
                      ),
                      itemBuilder: (context, index) {
                        return AnimationConfiguration.staggeredList(
                          position: index,
                          duration: const Duration(milliseconds: 375),
                          child: SlideAnimation(
                            verticalOffset: 50.0,
                            child: FadeInAnimation(
                              child: Card(
                                elevation: 0.2,
                                color: AppColors.White_COLOR,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(20.r)),
                                child: Padding(
                                  padding: EdgeInsets.symmetric(
                                    horizontal: 12.w,
                                  ),
                                  child: Column(
                                    children: [
                                      SizedBox(height: 10.h),
                                      Image.asset(
                                        listImage2[index],
                                        height: 127.h,
                                        width: 160.w,
                                        fit: BoxFit.fill,
                                      ),
                                      SizedBox(height: 10.h),
                                      MyText(
                                          text: 'هذا النص هو مثال لنص',
                                          fontSize: 16.sp,
                                          maxLines: 2,
                                          textAlign: TextAlign.center,
                                          fontWeight: FontWeight.w600,
                                          color: AppColors.BLack_COLOR),
                                      SizedBox(height: 5.h),
                                      Row(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        // mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          MyText(
                                              text: '120',
                                              fontSize: 20.sp,
                                              fontWeight: FontWeight.w600,
                                              color: AppColors.Green_COLOR),
                                          SizedBox(width: 7.w),
                                          MyText(
                                              text: 'دولار',
                                              fontSize: 12.sp,
                                              fontWeight: FontWeight.w400,
                                              color: AppColors.COLOR),
                                          Spacer(),
                                          Container(
                                            height: 31.h,
                                            width: 61.w,
                                            alignment: Alignment.center,

                                            decoration: BoxDecoration(
                                              color: AppColors.BackSel_COLOR,
                                              borderRadius:
                                              BorderRadius.circular(8.r),
                                            ),
                                            child: MyText(
                                                text: 'اسم المتجر',
                                                fontSize: 12.sp,
                                                fontWeight: FontWeight.w300,
                                                color: AppColors.Sel_COLOR),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        );
                      },
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
