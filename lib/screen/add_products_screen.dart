import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:task_desgin/models/categoriesRequset.dart';
import 'package:task_desgin/utils/app_colors.dart';
import 'package:task_desgin/utils/helpers.dart';
import 'package:task_desgin/widget/elevated_buttonA.dart';
import 'package:task_desgin/widget/my_text.dart';
import 'package:task_desgin/widget/tango_text_field.dart';

class AddProductsScreen extends StatefulWidget {
  const AddProductsScreen({Key? key}) : super(key: key);

  @override
  State<AddProductsScreen> createState() => _AddProductsScreenState();
}

class _AddProductsScreenState extends State<AddProductsScreen> {
  List<String> listImage1 = [
    'images/Rectangle1.png',
    'images/Rectangle2.png',
    'images/Rectangle3.png',
    'images/Rectangle1.png',
  ];
  TextEditingController nameProController = TextEditingController();
  final List<CategoriesRequset> items = [
    CategoriesRequset(id: 1, name: 'تصنيف 1'),
    CategoriesRequset(id: 2, name: 'تصنيف 2'),
    CategoriesRequset(id: 3, name: 'تصنيف 3'),
    CategoriesRequset(id: 4, name: 'تصنيف 4'),
  ];
  TextEditingController textEditingController = TextEditingController();
  TextEditingController dTextEditingController = TextEditingController();
  TextEditingController sTextEditingController = TextEditingController();

  late CategoriesRequset selectedCategoryReques = items.first;

  setCategoryRequest(CategoriesRequset data) {
    this.selectedCategoryReques = data;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.White_COLOR,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.White_COLOR,
        title: MyText(
            text: 'اضافة منتجات',
            fontSize: 20.sp,
            fontWeight: FontWeight.w600,
            color: AppColors.COLOR),
        leading: Container(
            height: 30.h,
            width: 40.w,
            margin: EdgeInsets.only(right: 10.w, bottom: 7.h, top: 7.h),
            decoration: BoxDecoration(
                color: AppColors.White_COLOR,
                borderRadius: BorderRadius.circular(12.r),
                boxShadow: [
                  BoxShadow(spreadRadius: 1.5, color: Colors.black12)
                ]),
            child: IconButton(
                onPressed: () => Get.back(),
                icon: Icon(
                  Icons.arrow_back,
                  color: AppColors.BLack_COLOR,
                ))),
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.w),
        child: ListView(
          children: [
            SizedBox(height: 8.h),
            MyText(
                text: 'صور المنتج',
                fontSize: 16.sp,
                fontWeight: FontWeight.w600,
                color: AppColors.COLOR),
            SizedBox(height: 14.h),
            SizedBox(
              height: 100.h,
              child: ListView.builder(
                  physics: ScrollPhysics(),
                  scrollDirection: Axis.horizontal,
                  itemCount: images.length == 0 ? 4 : images.length,
                  itemBuilder: (BuildContext context, int index) {
                    return AnimationConfiguration.staggeredList(
                      position: index,
                      duration: Duration(milliseconds: 375),
                      child: SlideAnimation(
                        verticalOffset: 50.0,
                        child: FadeInAnimation(
                          child: Stack(
                            children: [
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 3.w),
                                child: Container(
                                  width: 93.w,
                                  height: 100.h,
                                  decoration: BoxDecoration(
                                    color: AppColors.White_COLOR1,
                                    borderRadius: BorderRadius.circular(7.h),
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(7.r),
                                    child: _xFile == null
                                        ? Container(
                                            width: 100.w,
                                            height: 100.h,
                                            color: AppColors.White_COLOR1,
                                          )
                                        : Image.file(
                                            images[index],
                                            width: 93.w,
                                            height: 100.h,
                                            fit: BoxFit.fill,
                                          ),
                                  ),
                                ),
                              ),
                              Positioned(
                                left: 8.w,
                                top: 5.h,
                                child: images.isNotEmpty
                                    ? InkWell(
                                        onTap: () {
                                          images.removeAt(index);
                                          setState(() {});
                                        },
                                        child: Container(
                                            decoration: BoxDecoration(
                                              color: AppColors.Red_COLOR,
                                              shape: BoxShape.circle,
                                            ),
                                            child: Icon(
                                              Icons.close,
                                              color: AppColors.White_COLOR1,
                                              size: 20.r,
                                            )),
                                      )
                                    : SizedBox(),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  }),
            ),
            SizedBox(height: 14.h),
            ElevatedButtonA(
              onPressed: () async {
                if (images.length < 4) {
                  await pickImage();
                } else {
                  Helpers.showSnackBar(
                      context: context,
                      message: 'الحد المسموح 4 صور',
                      error: true);
                }
              },
              text: 'اضغط لاضافة الصور',
              icon: Icon(Icons.add_box_outlined),
              color: AppColors.Green_COLOR,
              textColors: AppColors.White_COLOR1,
            ),
            SizedBox(height: 26.h),
            MyText(
                text: 'اسم المنتج',
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: AppColors.BLack_COLOR),
            SizedBox(height: 6.h),
            TangoTextField(
              controller: nameProController,
              hint: 'اسم المنتج',
            ),
            SizedBox(height: 21.h),
            MyText(
                text: 'اسم المتجر',
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: AppColors.COLOR),
            SizedBox(height: 6.h),
            TangoTextField(
              controller: nameProController,
              hint: 'اسم المتجر',
            ),
            SizedBox(height: 21.h),
            MyText(
                text: 'السعر',
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: AppColors.BLack_COLOR),
            SizedBox(height: 6.h),
            TangoTextField(
              controller: nameProController,
              hint: 'السعر',
            ),
            SizedBox(height: 21.h),
            MyText(
                text: 'التصنيف',
                fontSize: 16.sp,
                fontWeight: FontWeight.w400,
                color: AppColors.BLack_COLOR),
            SizedBox(height: 6.h),
            Card(
              elevation: 0,
              color: AppColors.White_COLOR1,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(15.r)),
                  side: BorderSide(color: Colors.black26, width: 1)),
              child: Container(
                height: 60.h,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15.r),
                    boxShadow: [
                      BoxShadow(
                          spreadRadius: 1.5, color: AppColors.White_COLOR1)
                    ]),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<CategoriesRequset>(
                    borderRadius: BorderRadius.circular(15.r),
                    value: selectedCategoryReques,
                    isExpanded: true,
                    iconDisabledColor: AppColors.COLOR,
                    iconEnabledColor: AppColors.COLOR,
                    icon: Container(
                      margin: EdgeInsets.all(12.r),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: AppColors.White_COLOR1,
                          boxShadow: [
                            BoxShadow(color: Color(0xFF5973DE), spreadRadius: 1)
                          ]),
                      child: IconButton(
                        onPressed: () {},
                        icon: Icon(Icons.keyboard_arrow_down_outlined,
                            color: Color(0xFF5973DE)),
                      ),
                    ),
                    items: items
                        .map((e) => DropdownMenuItem(
                              value: e,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 20.w),
                                child: MyText(
                                    text: e.name,
                                    fontSize: 14.sp,
                                    fontWeight: FontWeight.w500,
                                    color: Color(0xFF5973DE)),
                              ),
                            ))
                        .toList(),
                    onChanged: (value) {
                      setCategoryRequest(value!);
                    },
                  ),
                ),
              ),
            ),
            SizedBox(height: 14.h),
            ElevatedButtonA(
              onPressed: () {},
              text: 'اضافه المنتج',
              icon: Icon(
                Icons.add_box_outlined,
                color: AppColors.Green_COLOR,
              ),
              color: AppColors.Green_COLOR,
              textColors: AppColors.White_COLOR1,
            ),
          ],
        ),
      ),
    );
  }

  XFile? _xFile;
  ImagePicker _picker = ImagePicker();
  List<File> images = [];

  Future<void> pickImage() async {
    _xFile =
        await _picker.pickImage(source: ImageSource.gallery, imageQuality: 10);
    if (_xFile != null) {
//      print("we are here");
//      print("Here : ${_xFile!.path}");
      setState(() {
        // print("******# : ${_xFile!.path}");
      });
      images.add(File(_xFile!.path));
    }
  }
}
