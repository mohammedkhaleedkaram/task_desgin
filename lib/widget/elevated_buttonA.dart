import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_desgin/utils/app_colors.dart';

class ElevatedButtonA extends StatelessWidget {
  final Function() onPressed;
  final String text;
  final Color color;
  final Color textColors;
  final Color sideColors;
  final int fontSize;
  final Widget icon;
  final bool? isLoading;

  ElevatedButtonA({
    required this.onPressed,
    required this.text,
    required this.color,
    this.textColors = Colors.black,
    this.sideColors = AppColors.Green_COLOR,
    this.fontSize = 14,
    this.isLoading = false,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return ElevatedButton.icon(
      icon: icon,
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
        primary: color,
        minimumSize: Size(double.infinity, 55.h),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.r),
          side: BorderSide(color: sideColors, width: 1),
        ),
      ),
      label: Text(
        text,
        style: TextStyle(
          fontSize: fontSize.sp,
          color: textColors,
          fontWeight: FontWeight.w500,
          fontFamily: 'Montserrat-Arabic',
        ),
      ),
    );
  }
}

// class ElevatedButtonc extends StatelessWidget {
//   final Function() onPressed;
//   final String text;
//   final Color color;
//   final Color textColors;
//   final Color sideColors;
//   final int fontSize;
//   late  bool? isLoading;
//
//   ElevatedButtonc({
//     required this.onPressed,
//     required this.text,
//     required this.color,
//     this.textColors = Colors.black,
//     this.sideColors = AppColors.COLOR2,
//     this.fontSize = 16,
//     this.isLoading ,
//   });
//
//   bool isL  = false;
//
//   @override
//   Widget build(BuildContext context) {
//     return ElevatedButton.icon(
//       icon:  isLoading!
//           ? Container(
//         width: 24,
//         height: 24,
//         padding: const EdgeInsets.all(2.0),
//         child: const CircularProgressIndicator(
//           color: AppColors.COLOR,
//           strokeWidth: 3,
//         ),
//       )
//           : Container(
//         height: 0,
//       ),
//       onPressed:  onPressed   ,
//       style: ElevatedButton.styleFrom(
//         primary: color,
//         minimumSize: Size(double.infinity, 55.h),
//         shape: RoundedRectangleBorder(
//           borderRadius: BorderRadius.circular(30.r),
//           side: BorderSide(color: sideColors, width: 1),
//         ),
//       ),
//       label: Text(
//         text,
//         style: TextStyle(
//             fontSize: fontSize.sp,
//             color: textColors,
//             fontWeight: FontWeight.bold,
//             fontFamily: 'Tajawal'),
//       ),
//     );
//   }
// }
