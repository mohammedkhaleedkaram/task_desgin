import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:task_desgin/utils/app_colors.dart';

import 'package:task_desgin/widget/my_text.dart';

class TangoTextField extends StatelessWidget {
  final String? hint;

  final TextInputType textInputType;
  final bool obscureText;
  final TextEditingController controller;
  final int maxLength;
  final double height1;
  final Widget? suffixText;
  final String? errorText;
  final IconData? prefixIcon;
  final Widget? prefixW;
  final Function? validation;

  final List<TextInputFormatter>? inputFormatter;

  TangoTextField({
    this.hint,
    this.suffixText,
    this.textInputType = TextInputType.text,
    this.obscureText = false,
    this.maxLength = 200,
    this.height1 =60,
    required this.controller,
    this.errorText,
    this.prefixIcon,
    this.validation,
    this.prefixW,
    this.inputFormatter,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height1.h,
      decoration: BoxDecoration(
        color: AppColors.White_COLOR1,
        borderRadius: BorderRadius.all(Radius.circular(15.r)),
      ),
      child: TextFormField(
        validator: (v) => this.validation!(v),
        keyboardType: textInputType,
        controller: controller,
        obscureText: obscureText,
        decoration: InputDecoration(
          prefix: prefixW,
          fillColor: AppColors.White_COLOR1,
          contentPadding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
          errorText: errorText,
          focusedErrorBorder: InputBorder.none,
          errorStyle: TextStyle(
              color: AppColors.Red_COLOR,
              fontFamily: 'Montserrat-Arabic',
              fontSize: 10.sp,
              fontWeight: FontWeight.w600),
          // labelText: labelText,
          // labelStyle:  GoogleFonts.montserrat(color: HexColor("#6e6b7b")),
          hintStyle: TextStyle(
              color: Colors.black38,
              fontFamily: 'Montserrat-Arabic',
              fontSize: 14.sp,
              fontWeight: FontWeight.w500),
          hintText: hint,
          // prefixIcon: icon,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.r)),
            borderSide: BorderSide(color: Colors.black26, width: 1),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.r)),
            borderSide: BorderSide(color: Colors.black26, width: 1),
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.r)),
            borderSide: BorderSide(color: Colors.black26, width: 1),
          ),
          errorBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(15.r)),
            borderSide: BorderSide(color: Colors.black26, width: 1),
          ),
        ),

        onSaved: (String? value) {
          // This optional block of code can be used to run
          // code when the user saves the form.
        },
        // onChanged:onchanged,
        // controller: controller,
      ),
    );
  }
}
/*Material(
      elevation: 0.6,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(25.r),
      ),
      child: SizedBox(
        height: height1,
        child:Column(
          children: [
            TextField(
              keyboardType: textInputType,
              inputFormatters: inputFormatter,
              maxLines: 10,
              obscureText: obscureText,
              controller: controller,

              // maxLength: maxLength,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(40.0),

                  ),
                  suffixIcon: suffixText,
                  suffixStyle: TextStyle(
                    color: AppColors.S_COLOR,
                    fontFamily:'Tajawal' ,
                  ),

                  filled: true,
                  hintStyle: TextStyle(
                      color: Colors.black38,
                      fontFamily: 'Tajawal',
                      fontSize: 15.sp,
                      fontWeight: FontWeight.w400),
                  hintText: '${hint}',
                  // label: Text('Type in your text'),
                  prefixIcon:
                  prefixW ?? Icon(prefixIcon, color: AppColors.White_COLOR),

                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.r),
                      borderSide: BorderSide(color: Colors.transparent)),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(25.r),
                      borderSide: BorderSide(color: Colors.transparent)),
                  fillColor: AppColors.S_COLOR),

            ),
            Material(
              elevation: 0.6,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(25.r),
              ),
              child: TextFormField(
                keyboardType:textInputType,
                controller: controller,
                decoration: InputDecoration(

                    contentPadding:EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
                    errorText:'errortext',
                    // labelText: labelText,
                    // labelStyle:  GoogleFonts.montserrat(color: HexColor("#6e6b7b")),
                    // hintStyle: GoogleFonts.montserrat(),
                    hintText: hint,
                    // prefixIcon: icon,
                    focusedBorder: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(30.0)),borderSide: BorderSide(color: Colors.transparent)),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        borderSide: BorderSide(color: Colors.transparent, width: 1)),
                    errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(30.0)),
                        borderSide: BorderSide(color: Colors.transparent, width: 1))),
                onSaved: (String? value) {
                  // This optional block of code can be used to run
                  // code when the user saves the form.
                },
                // onChanged:onchanged,
                // controller: controller,
              ),
            ),
          ],
        ),
      ),
    );*/
/*
* TextFormField(
//          validator: (v) => this.validation!(v),
          keyboardType: textInputType,
          inputFormatters: inputFormatter,
          maxLines: 10,
          obscureText: obscureText,
          controller: controller,
          maxLength: maxLength,
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            hintText: hint,
            // label: MyText(text: lablee.toString(), fontSize: 12.sp, fontWeight: FontWeight.w300, color: Colors.black38,),
            // errorText: errorText,
            errorText: errorText,
            fillColor: AppColors.S_COLOR,
            filled: true,
            counterText: '',

            // counter: SizedBox.,

            prefixIcon:
                prefixW ?? Icon(prefixIcon, color: AppColors.White_COLOR),

            contentPadding: EdgeInsets.symmetric(
              horizontal: 25.w,
              vertical: 16.h,
            ),
            hintStyle: TextStyle(
                color: Colors.black38,
                fontFamily: 'Tajawal',
                fontSize: 15.sp,
                fontWeight: FontWeight.w300),
            // suffix: Text('Forgot'),
            suffixIcon: suffixText,
            suffixStyle: TextStyle(
              color: AppColors.S_COLOR,
              fontFamily: Translation() == "en" ? 'Tajawal' : "Roboto",
            ),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.r),
                borderSide: BorderSide(color: Colors.transparent)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(25.r),
                borderSide: BorderSide(color: Colors.transparent)),
          ),
        )*/
