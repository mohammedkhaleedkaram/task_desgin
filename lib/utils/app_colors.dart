import 'package:flutter/material.dart';

class AppColors {
  static const COLOR = Color(0xFF3E3E68);
  static const BLack_COLOR = Color(0xFF000000);
  static const White_COLOR = Color(0xFFF5F5F5);
  static const White_COLOR1 = Color(0xFFFFFFFF);
  static const Back_Ground_COLOR = Color(0xFFFFFFFF);
  static const Green_COLOR = Color(0xFF3EB86F);
  static const Red_COLOR = Color(0xFFFF4155);
  static const Sel_COLOR = Color(0xFFA1A1A1);
  static const BackSel_COLOR = Color(0xFFEEEEEE);
  static const TextFel_COLOR = Color(0xFFE1E1E1);

}
