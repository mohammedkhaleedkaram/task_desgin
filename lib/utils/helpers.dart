
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:task_desgin/utils/app_colors.dart';
import 'package:task_desgin/widget/my_text.dart';


class Helpers {
  static void showSnackBar(
      {required BuildContext context,
      required String message,
      bool error = false}) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: MyText(
            text: message,
            fontSize: 15.sp,
            maxLines: 2,
            fontWeight: FontWeight.bold,
            color: AppColors.White_COLOR),
        behavior: SnackBarBehavior.floating,
        backgroundColor: error ? AppColors.Red_COLOR : AppColors.Green_COLOR,
      ),
    );
  }
}
